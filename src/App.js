import React, {Component} from 'react';
import './App.less';
import {BrowserRouter} from "react-router-dom";
import {Route, Switch} from "react-router";
import WebHeader from "./components/WebHeader/WebHeader";
import AddCommodity from "./components/AddCommodity/AddCommodity";
import CommodityShop from "./components/CommodityShop/CommodityShop";
import Order from "./components/Order/Order";
import { Layout } from 'antd';
const { Footer } = Layout;
import 'antd/dist/antd.css';

class App extends Component {
  render() {
    return (
      <div id='root'>
        <BrowserRouter>
          <WebHeader/>
          <Switch>
            <Route exact path='/create' component={ AddCommodity }/>
            <Route exact path='/orders' component={ Order }/>
            <Route exact path='/' component={ CommodityShop }/>
          </Switch>
        </BrowserRouter>
        <Footer style={{ textAlign: 'center' }}>TW Mail ©2018 Created by TechieLiang</Footer>
      </div>
    );
  }
}

export default App;