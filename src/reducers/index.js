import {combineReducers} from "redux";
import addCommodity from "../components/AddCommodity/reducers/addCommodityReducer"
import getCommodities from "../components/CommodityShop/reducers/getCommonditiesRecucer"
import addOrder from "../components/CommodityShop/reducers/addOrderReducer"
import getOrders from "../components/Order/reducers/getOrdersReducer"
const reducers = combineReducers({
  addCommodity,
  getCommodities,
  addOrder,
  getOrders
});
export default reducers;