import {REDUX_GET_ORDERS_TYPE} from "../actions/getOrdersAction";

const initState = {
  orders: []
};

export default (state = initState, action) => {
  if (action.type === REDUX_GET_ORDERS_TYPE) {
    return {
      ...state,
      orders: action.payload
    };
  } else {
    return state
  }
};
