import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import {getOrders} from "./actions/getOrdersAction";
import {Link} from "react-router-dom";
import {fetchToDeleteOrder} from "./fetchToDelete";
import {Button, Table} from 'antd';

class Order extends Component {

  constructor(props, context) {
    super(props, context);
    this.handlerDeleteOrder = this.handlerDeleteOrder.bind(this);
  }

  componentDidMount() {
    this.props.getOrders();
  }

  handlerDeleteOrder(commodityId) {
    fetchToDeleteOrder(commodityId)
      .then(() => {
        this.props.getOrders();
      })
      .catch(errorData => alert("订单删除异常：" + errorData));
  }

  render() {
    let orders = this.props.orders.map(order => {
      return (<tr key={order.id}>
        <td className="commodity-name">{order.name}</td>
        <td className="commodity-price">{order.price}</td>
        <td className="commodity-number">{order.number}</td>
        <td className="commodity-unit">{order.unit}</td>
        <button onClick={this.handlerDeleteOrder.bind(this, order.id)}>删除</button>
      </tr>);
    });

    const ordersTableRows = this.props.orders.map(order => (
      {
        key: order.id,
        name: order.name,
        price: order.price,
        number: order.number,
        unit: order.unit
      }
    ));

    const ordersTableHeader = [
      {
        title: '名字',
        dataIndex: 'name',
        key: 'name',
      }, {
        title: '单价',
        dataIndex: 'price',
        key: 'price',
      }, {
        title: '数量',
        dataIndex: 'number',
        key: 'number',
      }, {
        title: '单价',
        dataIndex: 'unit',
        key: 'unit',
      }, {
        title: '操作',
        key: 'action',
        render: text => (
          <Button type="danger" onClick={this.handlerDeleteOrder.bind(this, text.key)}>删除</Button>
        ),
      },
    ];

    return (
      <main id='shop-order'>
        {orders.length === 0
          ? <Link to="/">
            <Button>无订单，请先购买</Button>
          </Link>
          : <Table dataSource={ordersTableRows} columns={ordersTableHeader}/>
        }
      </main>
    );
  }
}

const mapStateToProps = state => {
  return {
    orders: state.getOrders.orders,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  getOrders
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Order);