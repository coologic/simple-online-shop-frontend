export const REDUX_GET_ORDERS_TYPE = 'GET_ORDERS';

export const getOrders = () => dispatch => {
  fetch('http://localhost:8080/api/orders', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
  })
    .then((data) => {
      if (data.ok === true) {
        return data.json();
      } else {
        alert('post to add data error' + JSON.stringify(data));
      }
    })
    .then(jsonData => {
      dispatch(
        {
          type: REDUX_GET_ORDERS_TYPE,
          payload: jsonData
        });
    })
    .catch(errorData => alert(errorData));
};