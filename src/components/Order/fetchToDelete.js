export const fetchToDeleteOrder = commodityId => {
  return fetch(`http://localhost:8080/api/orders/${commodityId}`, {
    method: 'delete',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
  })
    .then((data) => {
      if (data.ok) {
        return data;
      } else {
        alert('删除订单失败：' + JSON.stringify(data));
      }
    });
};