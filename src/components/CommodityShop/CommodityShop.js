import React, {Component} from 'react';
import {getCommodities} from "./actions/getCommonditiesAction";
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import {postToAddOrder} from "./actions/addOrderAction";
import {Link} from "react-router-dom";
import {Card, Button, Row, Col} from 'antd';

const {Meta} = Card;

class CommodityShop extends Component {

  constructor(props, context) {
    super(props, context);
    this.handlerOrder = this.handlerOrder.bind(this);
  }

  componentDidMount() {
    this.props.getCommodities();
  }

  handlerOrder(commodityId, commodityName) {
    this.props.postToAddOrder(commodityId, commodityName);
  }

  render() {
    const commodities = this.props.commodities.map(commodity => {
      return (<Col span={5} key={commodity.id} style={{margin:'32px 0'}}><Card
        hoverable
        cover={<img height="120px"
                    alt="pic"
                    src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1565118699090&di=872bfde4c9762864ddbd74622b748415&imgtype=0&src=http%3A%2F%2Fpic1.nipic.com%2F2009-02-24%2F200922416145790_2.jpg"/>}
      >
        <Meta title={commodity.name} description={commodity.price + "/" + commodity.unit}/>
        <Button onClick={this.handlerOrder.bind(this, commodity.id, commodity.name)}>ADD</Button>
      </Card></Col>);
    });

  return (
      <main id='commodity-shop'>
        {commodities.length > 0
          ? <Row type="flex" justify="space-between">{commodities}</Row>
          : <Link to="/create"><Button>无物品，请添加</Button></Link>
        }
      </main>
    );
  }
}

CommodityShop.propTypes = {};


const mapStateToProps = state => {
  return {
    commodities: state.getCommodities.commodities,
    byThisPropsToRefreshWhenAddOrderSuccess: state.addOrder.orderAdded
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  getCommodities,
  postToAddOrder
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CommodityShop);