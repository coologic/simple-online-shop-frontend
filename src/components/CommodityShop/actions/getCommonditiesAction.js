export const REDUX_GET_COMMODITIES_TYPE = 'GET_COMMODITIES';

export const getCommodities = () => dispatch => {
  fetch('http://localhost:8080/api/commodities', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
  })
    .then((data) => {
      if (data.ok === true) {
        return data.json();
      } else {
        alert('post to add data error' + JSON.stringify(data));
      }
    })
    .then(jsonData => {
      dispatch(
        {
          type: REDUX_GET_COMMODITIES_TYPE,
          payload: jsonData
        });
    })
    .catch(errorData => alert(errorData));
};