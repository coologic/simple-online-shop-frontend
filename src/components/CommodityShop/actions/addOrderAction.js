export const REDUX_ADD_ORDER_TYPE = 'ADD_ORDER';

export const postToAddOrder = (commodityId, commodityName) => dispatch => {
  fetch('http://localhost:8080/api/buy', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    body: JSON.stringify({
      id:commodityId,
      name:commodityName,
      number:1
    }),
  })
    .then(data => {
      if (data.ok === true) {
        dispatch(
          {
            type: REDUX_ADD_ORDER_TYPE,
            payload: commodityId
          });
      } else {
        alert('post to add data error' + JSON.stringify(data));
      }
    })
    .catch(errorData => alert(errorData));
};