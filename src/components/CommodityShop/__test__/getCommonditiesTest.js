// import React from 'react';
//
// import '@testing-library/jest-dom/extend-expect';
// import '@testing-library/react/cleanup-after-each';
//
// import { render, fireEvent } from '@testing-library/react';
//
// import axios from 'axios';
// import 'babel-polyfill';
//
// import CommodityShop from "../CommodityShop";
//
// jest.mock('axios'); // Mock axios模块
//
// test('Order组件显示异步调用订单数据', async () => {
//   axios.get.mockResolvedValue([{id:1,name:"name",price:100,unit:"个"}]);
//   const { getByLabelText, findByTestId } = render(<CommodityShop />);
//   fireEvent.change(getByLabelText('number-input'), '1');
//   await expect(findByTestId('commodities-data-ul')).resolves.toHaveTextContent('name');
// });

//TODO 没做过对fetch的例子，考虑使用https://www.npmjs.com/package/jest-fetch-mock的例子
//暂时不进行测试
