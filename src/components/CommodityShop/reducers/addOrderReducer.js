import {REDUX_ADD_ORDER_TYPE} from "../actions/addOrderAction";

const initState = {
  commodities: []
};

export default (state = initState, action) => {
  if (action.type === REDUX_ADD_ORDER_TYPE) {
    return {
      ...state,
      orderAdded: action.payload
    };
  } else {
    return state
  }
};
