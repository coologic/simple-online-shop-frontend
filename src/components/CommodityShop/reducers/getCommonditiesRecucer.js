import {REDUX_GET_COMMODITIES_TYPE} from "../actions/getCommonditiesAction";

const initState = {
  commodities: []
};

export default (state = initState, action) => {
  if (action.type === REDUX_GET_COMMODITIES_TYPE) {
    return {
      ...state,
      commodities: action.payload
    };
  } else {
    return state
  }
};
