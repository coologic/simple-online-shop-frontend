import React, {Component} from 'react';
import "./WebHeader.less"
import {Link} from "react-router-dom";
import { Layout, Menu, Icon } from 'antd';
const { Header } = Layout;


class WebHeader extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {};
  }

  render() {
    return (
      <Header>
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={['1']}
          style={{ lineHeight: '64px' }}
        >
          <Menu.Item key="1"><Link to="/"><Icon type="pie-chart" />商城</Link></Menu.Item>
          <Menu.Item key="2"><Link to = "/orders"><Icon type="snippets" />订单</Link></Menu.Item>
          <Menu.Item key="3"><Link to="/create"><Icon type="plus" />添加商品</Link></Menu.Item>
        </Menu>
      </Header>
    );
  }
}

export default WebHeader;
