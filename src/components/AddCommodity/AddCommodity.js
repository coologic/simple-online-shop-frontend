import React, {Component} from 'react';
import {postToAddCommodity} from "./actions/addCommodityAction";
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import { Layout, Input, Button} from 'antd';
const { Content } = Layout;


class AddCommodity extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      title: "",
      price: 0.0,
      unit: "",
      pictureUrl: ""
    };
    this.handleAddCommodity = this.handleAddCommodity.bind(this);
    this.handleNameChanged = this.handleNameChanged.bind(this);
    this.handlePriceChanged = this.handlePriceChanged.bind(this);
    this.handleUnitChanged = this.handleUnitChanged.bind(this);
    this.handlePictureUrlChanged = this.handlePictureUrlChanged.bind(this);
  }

  handleAddCommodity() {
    const commodity = {
      name:this.state.name,
      price:this.state.price,
      unit:this.state.unit,
      pictureUrl:this.state.pictureUrl
    };
    this.props.postToAddCommodity(commodity);
    // this.props.history.push("/");
  }

  handleNameChanged(event) {
    this.setState({
      name: event.currentTarget.value
    });
  }

  handlePriceChanged(event) {
    this.setState({
      price: event.currentTarget.value
    });
  }
  handleUnitChanged(event) {
    this.setState({
      unit: event.currentTarget.value
    });
  }
  handlePictureUrlChanged(event) {
    this.setState({
      pictureUrl: event.currentTarget.value
    });
  }

  render() {
    return (
      <Content id='add-note' style={{ padding: '0 50px', margin:'10px 0' }}>
        <label className='label-title'>name:</label>
        <Input size="large" placeholder="title" className='input-title' value={this.state.name} onChange={this.handleNameChanged}/>
        <br/>
        <label className='label-title'>price:</label>
        <Input size="large" placeholder="price" className='input-title' value={this.state.price} onChange={this.handlePriceChanged}/>
        <br/>
        <label className='label-title'>unit:</label>
        <Input size="large" placeholder="unit" className='input-title' value={this.state.unit} onChange={this.handleUnitChanged}/>
        <br/>
        <label className='label-title'>url:</label>
        <Input size="large" placeholder="pictureUrl" className='input-title' value={this.state.pictureUrl} onChange={this.handlePictureUrlChanged}/>
        <br/>
        <Button type="primary" onClick={this.handleAddCommodity}>确定</Button>
      </Content>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  postToAddCommodity,
}, dispatch);

export default connect(null, mapDispatchToProps)(AddCommodity);