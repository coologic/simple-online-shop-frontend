import {REDUX_ADD_COMMODITY_TYPE} from "../actions/addCommodityAction";

const initState = {
  addCommodityState: {}
};

export default (state = initState, action) => {
  if (action.type === REDUX_ADD_COMMODITY_TYPE) {
    return {
      ...state,
      addCommodityState: action.payload
    };
  } else {
    return state
  }
};
