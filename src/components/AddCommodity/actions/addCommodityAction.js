export const REDUX_ADD_COMMODITY_TYPE = 'ADD_COMMODITY';

export const postToAddCommodity = commodity => dispatch => {
  fetch('http://localhost:8080/api/commodities', {
    method: 'POST',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    body: JSON.stringify({
      name:commodity.name,
      price:commodity.price,
      unit:commodity.unit,
      pictureUrl:commodity.pictureUrl
    })
  })
    .then(data => {
      if (data.ok === true) {
        dispatch(
          {
            type: REDUX_ADD_COMMODITY_TYPE,
            payload: data
          });
      } else {
        alert('post to add data error' + JSON.stringify(data));
      }
    })
    .catch(errorData => alert(errorData));
};